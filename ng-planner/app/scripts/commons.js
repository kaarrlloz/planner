/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var common = angular.module('common', ['restangular']);
var host = location.hostname;
common.config(['RestangularProvider', function (RestangularProvider) {
    RestangularProvider.setBaseUrl('http://'+host+':3001/api/');
    RestangularProvider.setDefaultHttpFields({ cache: true });
}]);

common.factory('Mensajes', [function Mensajes() {
    return {

        showMessage: function (mensaje) {
            let color = mensaje.type
            $.notify({
        	icon: "notifications",
        	message: mensaje.msg

        },{
            type: type[color],
            timer: 4000,
            placement: {
                from: 'top',
                align: 'right'
            }
        });
                
        }
    }
}]);

common.service('Properties', function () {
    this.getMensaje = function (mensaje, values) {
        for (var i = 0; i < values.length; ++i) {
            mensaje = mensaje.replace('{' + i + '}', values[i]);          
        }
        return mensaje;
    };

    this.isUndNull = function (val) {
        if (val !== undefined && val !== null) {
            return val;
        } else {
            return '--';
        }
    };
});

common.factory('ListaTareas', ['Restangular', function ListaTareas(Restangular) {
    return {
        tareas: function (filtro) {
            return Restangular.all('tarea').customGET("", "");
        }
    }
}]);