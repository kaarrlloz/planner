var app = angular.module('app', ['restangular','common']);


app.controller('loginController', ['$http', '$rootScope', '$scope','Restangular',
    function ($http, $rootScope, $scope, Restangular) {

        $scope.user = {
            email: "",
            password: ""
        }

        $scope.logIn = function () {

            Restangular.all('signIn').post($scope.user).then(
                function (result) {
                    localStorage["token"] = result.token;
                    localStorage["sisDisplayName"] = result.nombre;
                    window.location = "index.html";
                }
            ).catch( result => {
                //manejo de error de autenticacion
                $scope.user = {
                    email: "",
                    password: ""
                }
            });

        }


    }]);