(function () {
    'use strict';


    var app = angular.module('app', ['ngRoute', 'common', 'app.directives',
        'datetimepicker', 'app.tareas', 'app.priorizacion', 'app.seguimiento', 'app.board']);

    app.controller('AppController', ['$rootScope', '$scope', '$http', AppController]);

    function AppController($rootScope, $scope, $http) {

        $scope.listaMenu =
            [{
                tituloMenu: 'Crear tareas',
                title: '',
                rutaMenu: 'verTareas'
            },
            {
                tituloMenu: 'Tablero',
                title: '',
                rutaMenu: 'board'
            },
            {
                tituloMenu: 'Priorizar tareas',
                title: '',
                rutaMenu: 'priorizar'
            },
            {
                tituloMenu: 'Seguimiento',
                title: '',
                rutaMenu: 'seguimiento'
            }
            ];

        /*if (localStorage["active"] != "true") {
            window.location = "login.html";
        } else {
            $http({
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json'
                },
                url: 'http://localhost:8888/api/commons/menu'
            }).success(function (data) {
                if (data) {
                    $scope.menu = data
                }
            }).error(function (data) {
                console.log("Doesn't work");
            });
        }*/

        $scope.logOut = function () {
            localStorage["token"] = null;
            localStorage["sisDisplayName"] = null;
            window.location = "login.html";
        }

        $rootScope.$on('$routeChangeSuccess', function (next, current) {
            $(window).scrollTop(0);
            $(".active").removeClass("active");
            var url = window.location;
            var urlseparada = url.href.split('/');
            var element = $('ul.nav a').filter(function () {
                if (this.href != "") {
                    var urlseparadaelemento = this.href.split('/');
                    return this.href == url || urlseparada[urlseparada.length - 1].indexOf(urlseparadaelemento[urlseparadaelemento.length - 1]) == 0;
                }

            }).addClass('active').addClass('in').parent();
            if (element.is('li')) {
                element.addClass('active');
            }
        });

        $rootScope.$on("$routeChangeStart", function (event, next, current, $route) {
            //funcion al terminar el enrutamiento
        });


        $scope.dispalyname = localStorage["sisDisplayName"]




    }

    app.directive('myPostRepeatDirective', function () {
        return function ($scope) {
            if ($scope.$last) {
                $('#side-menu').metisMenu();
            }
        };
    });

    app.config(['$httpProvider', '$routeProvider', 'datetimepickerProvider', function ($httpProvider, $routeProvider, datetimepickerProvider) {

        var mindate = new Date();
        var minimo = new Date(mindate);
        minimo = mindate.setFullYear(1900);

        datetimepickerProvider.setOptions({
            locale: 'es',
            format: 'YYYY/MM/DD',
            useCurrent: false,
            showTodayButton: true,
            minDate: minimo,
            focusOnShow: false,
            showClose: true
        });

        $routeProvider.
            when('/', {
                templateUrl: '../components/lista_tareas/lista_tareas.html',
                controller: 'ListaTareasController'
            }).
            when('/standar', {
                templateUrl: '../components/standar/standar.html',
                controller: 'StandarController'
            }).

            when('/verTareas', {
                templateUrl: '../components/lista_tareas/lista_tareas.html',
                controller: 'ListaTareasController'
            }).
            when('/priorizar', {
                templateUrl: '../components/priorizacion/priorizacion.html',
                controller: 'PriorizacionController'
            }).
            when('/seguimiento', {
                templateUrl: '../components/seguimiento/seguimiento.html',
                controller: 'SeguimientoController'
            }).
            when('/board', {
                templateUrl: '../components/board/board.html',
                controller: 'BoardController'
            }).

            otherwise({
                redirectTo: '/login.html'
            });

        $httpProvider.interceptors.push(['$q', function ($q) {
            return {
                'request': function (config) {
                    config.headers = config.headers || {};

                    //if ($localStorage.token) {
                    if (localStorage["token"]) {
                        //config.headers.Authorization = $localStorage.token;
                        config.headers.Authorization = 'Bearer ' + localStorage["token"];
                    }
                    return config;
                },
                'responseError': function (response) {
                    if (response.status === 401 || response.status === 403 || response.status === 409) {
                        //$location.path('/');
                        window.location = "login.html";
                    }
                    return $q.reject(response);
                }
            };
        }]);

    }]);



})();