var app = angular.module('app', ['restangular','common']);


app.controller('signupController', ['$http', '$rootScope', '$scope','Restangular','Mensajes',
    function ($http, $rootScope, $scope, Restangular,Mensajes) {

        $scope.user = {
            email: "",
            nombre: "",
            password: ""
        }
        $scope.password = ""

        $scope.signUp = function () {
            if($scope.password != $scope.user.password){
                Mensajes.showMessage({type:4, msg:"Los password no cinsiden"});
                return;
            }

            Restangular.all('signUp').post($scope.user).then(
                function (result) {
                    localStorage["token"] = result.token;
                    window.location = "index.html";
                }
            ).catch( result => {
                //manejo de error de autenticacion
                $scope.user = {
                    email: "",
                    nombre: "",
                    password: ""
                }
                $scope.password = ""
                Mensajes.showMessage({type:4, msg:"<b>Sis-Planner</b> -- No se pudo crear el usuario el <b>email</b> ya se encuentra registrado"});
            });

        }


    }]);