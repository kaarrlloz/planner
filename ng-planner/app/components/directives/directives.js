﻿// Code goes here
var app = angular.module('app.directives', []);



app.directive('loading',   ['$http' ,function ($http)
{
    return {
        restrict: 'A',
        link: function (scope, elm, attrs)
        {
            scope.isLoading = function () {
                return $http.pendingRequests.length > 0;
            };

            scope.$watch(scope.isLoading, function (v)
            {
                if(v){
                    elm.show();
                }else{
                    elm.hide();
                }
            });
        }
    };

}]);

//Captura Enter
app.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
                document.getElementById('text2').focus();
            }
        });
    };
});


//Doble Input
app.directive("dobleInput", function () {
    return {
        restrict: "E",
        scope: {
            ngModel: "=",
            hbtChange: "&",
            hbtRequired: "=",
            hbtLimpiar: "=",
            hbtId: "=",
            size: "="
        },
        link: function ($scope, element, attrs) {
            $scope.render = false;
            $scope.mostrarmensaje = false;
            $scope.fine = true;
            $scope.mensag = "";
            $scope.valor1 = "";

            if ($scope.size == undefined) {
                $scope.size = 15;
            }

            $scope.hbtLimpiar = function () {
                $scope.render = false;
                $scope.mostrarmensaje = false;
                $scope.fine = true;
                $scope.mensag = "";
                $scope.valor1 = "";
            }

            $scope.cambiar = function (idInput) {
                if ($scope.ngModel != "") {
                    $scope.render = true;
                    $scope.valor1 = $scope.ngModel
                    $scope.ngModel = null;
                    
                    setTimeout(function timeoutFunction() {$('#' + idInput).focus();}, 100);
                }                                              
            };


            $scope.confirmar = function (idInput) {
                if ($scope.valor1 == $scope.ngModel) {
                    $scope.mostrarmensaje = false;
                    $scope.fine = true;
                    $scope.hbtChange();
                } else {
                    $scope.mensag = "Los datos no coinciden";
                    $scope.mostrarmensaje = true;
                    $scope.render = false;
                    $scope.ngModel = null;
                    $scope.fine = false;
                    setTimeout(function timeoutFunction() { $('#' + idInput).focus(); }, 100);
                }
            };
        },
        templateUrl: 'components/directives/doble-input.html'
    }
});

//Captura solo números
app.directive('numbersOnly', function () {
    return {
        require: 'ngModel',
        scope: {
            size: "="
        },
        link: function ($scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                if (inputValue == undefined) return ''
                var transformedInput = inputValue.replace(/[^0-9]/g, '');
                if ($scope.size) {
                    if (transformedInput.length > $scope.size) {
                        transformedInput = transformedInput.substring(0, $scope.size);
                    }
                }
                if (transformedInput != inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }
                return transformedInput;
            });
        }
    };
});

//directiva para campos varchar o alfanumericos
app.directive('varchar', function() {
    return {
        require: 'ngModel',
        scope: {
            size: "="
        },
        link: function ($scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                var transformedInput = text.replace(/[^0-9a-zA-ZÑñáéíóúÁÉÍÓÚ\-\s]/g, '');
                //transformedInput = text.replace(/^\s/g, '');
                if ($scope.size) {
                    if (transformedInput.length > $scope.size) {
                        transformedInput = transformedInput.substring(0, $scope.size);
                    }
                }

                if (transformedInput !== text) {
                    ngModelCtrl.$setViewValue(transformedInput);
                    ngModelCtrl.$render();
                }
                return transformedInput;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});

app.directive('documento', function () {
    return {
        require: 'ngModel',
        scope: {
            docMatch: "="
        },
        link: function ($scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                var match = new RegExp($scope.docMatch);
               // var transformedInput = text.replace(new RegExp(match), '');
                var transformedInput = text.replace(/[^1-9]+[^\d]*/g, '');

                if (transformedInput !== text) {
                    ngModelCtrl.$setViewValue(transformedInput);
                    ngModelCtrl.$render();
                }
                return transformedInput;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});

app.directive('dragSelect', function ($window, $document) {
    return {
        scope: {
            dragSelectIds: '=',
            clear:'='
        },
        controller: function ($scope, $element) {
            var cls = 'eng-selected-item';
            var startCell = null;
            var dragging = false;
            $scope.dragSelectIds = [];

            function mouseUp(el) {
                dragging = false;
            }

            function mouseDown(el) {
                dragging = true;
                setStartCell(el);
                setEndCell(el);
            }

            function mouseEnter(el) {
                if (!dragging) return;
                setEndCell(el);
            }

            function setStartCell(el) {
                startCell = el;
            }

            function setEndCell(el) {
                cellsBetween(startCell, el).each(function () {
                    var el = angular.element(this);
                    var noexiste = $.grep($scope.dragSelectIds, function (obj) {return (obj == el.attr('id'));});

                    if (noexiste.length === 0) {
                        el.addClass(cls);
                        $scope.dragSelectIds.push(el.attr('id'));
                    } else {
                        el.removeClass(cls);
                        $scope.dragSelectIds = $.grep($scope.dragSelectIds,
                        function (obj, i) { return (obj == el.attr('id')); }, true);
                    }
                });             
            }

            function cellsBetween(start, end) {
                var coordsStart = getCoords(start);
                var coordsEnd = getCoords(end);
                var topLeft = {
                    column: $window.Math.min(coordsStart.column, coordsEnd.column),
                    row: $window.Math.min(coordsStart.row, coordsEnd.row),
                };
                var bottomRight = {
                    column: $window.Math.max(coordsStart.column, coordsEnd.column),
                    row: $window.Math.max(coordsStart.row, coordsEnd.row),
                };
                return $element.find('td').filter(function () {
                    var el = angular.element(this);
                    var coords = getCoords(el);
                    return coords.column >= topLeft.column
                        && coords.column <= bottomRight.column
                        && coords.row >= topLeft.row
                        && coords.row <= bottomRight.row;
                });
            }

            function getCoords(cell) {
                var row = cell.parents('row');
                return {
                    column: cell[0].cellIndex,
                    row: cell.parent()[0].rowIndex
                };
            }

            function wrap(fn) {
                return function () {
                    var el = angular.element(this);
                    $scope.$apply(function () {
                        fn(el);
                    });
                }
            }

            $scope.clear = function () {
                $scope.dragSelectIds = [];
                $element.find('td').removeClass(cls);
            }

            $element.delegate('td', 'mousedown', wrap(mouseDown));
            $element.delegate('td', 'mouseenter', wrap(mouseEnter));
            $document.delegate('body', 'mouseup', wrap(mouseUp));
        }
    }
});

app.directive('currencyMask', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        scope: {
            size: "="
        },
        link: function ($scope, element, attrs, ngModelController) {
            // Run formatting on keyup
            var numberWithCommas = function (value, addExtraZero) {

                ngModelController.$modelValue = ngModelController.$modelValue.substring(0, $scope.size);

                if (addExtraZero == undefined)
                    addExtraZero = false
                value = value.toString();
                value = value.replace(/[^0-9\,]/g, "");
                var lastChar = value.substr(value.length - 1);
                if (lastChar == ',') {
                    if (value.length == 1) {
                        value = value.substring(0, value.length - 1);
                    } else if (value.length > 1) {
                        lastChar = value.substr(value.length - 2);
                        if (lastChar == ',,') {
                            value = value.substring(0, value.length - 1);
                        }
                    }
                }
                var parts = value.split(',');
                if (parts.length > 2) {
                    value = value.substring(0, value.length - 1);
                    parts = value.split(',');
                }
                parts[0] = parts[0].replace(/\d{1,3}(?=(\d{3})+(?!\d))/g, "$&.");
                if (parts[1] && parts[1].length > 2) {
                    parts[1] = parts[1].substring(0, 2);
                }
                if (addExtraZero && parts[1] && (parts[1].length === 1)) {
                    parts[1] = parts[1] + "0" 
                }
                return parts.join(",");
            };

           

            var applyFormatting = function() {
                var value = element.val();
                var original = value;
                if (!value || value.length == 0) { return }
                value = numberWithCommas(value);
                if (value != original) {
                    element.val(value);
                    element.triggerHandler('input')
                }
            };
            element.bind('keyup', function(e) {
                var keycode = e.keyCode;
                var isTextInputKey = 
                  (keycode > 47 && keycode < 58)   || // number keys
                  keycode == 32 || keycode == 8    || // spacebar or backspace
                  (keycode > 64 && keycode < 91)   || // letter keys
                  (keycode > 95 && keycode < 112)  || // numpad keys
                  (keycode > 185 && keycode < 193) || // ;=,-./` (in order)
                  (keycode > 218 && keycode < 223);   // [\]' (in order)
                if (isTextInputKey) {
                    applyFormatting();
                }
            });
            ngModelController.$parsers.push(function(value) {
                if (!value || value.length == 0) {
                    return value;
                }
                value = value.toString();
                value = value.replace(/[^0-9\,]/g, "");
                value = value.substring(0, $scope.size);
                return value;
            });
            ngModelController.$formatters.push(function(value) {
                if (!value || value.length == 0) {
                    return value;
                }
                value = numberWithCommas(value, true);
                return value;
            });
        }
    };
});

app.directive('decimal', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModelController) {
            // Run formatting on keyup
            var numberWithCommas = function (value, addExtraZero) {
                if (addExtraZero == undefined)
                    addExtraZero = false
                value = value.toString();
                value = value.replace(/[^0-9\,]/g, "");
                var lastChar = value.substr(value.length - 1);
                if (lastChar == ',') {
                    if (value.length == 1) {
                        value = value.substring(0, value.length - 1);
                    } else if (value.length > 1) {
                        lastChar = value.substr(value.length - 2);
                        if (lastChar == ',,') {
                            value = value.substring(0, value.length - 1);
                        }
                    }
                }
                var parts = value.split(',');
                if (parts.length > 2) {
                    value = value.substring(0, value.length - 1);
                    parts = value.split(',');
                }
                parts[0] = parts[0].replace(/\d{3,3}(?=(\d{1})+(?!\d))/g, "$&,");
                if (parts[0] && parts[0].length > 3) {
                    var decparts = '';
                    var entparts = parts[0].split(',');
                    if (parts[1] && parts[1].length <= 3) {
                        decparts = parts[1].substring(0, (parts[1].length-1));
                    }
                    if (entparts[1] && decparts != '') {
                        parts[0] = parts[0].substring(0, 3);
                        parts[1] = entparts[1].concat(decparts);
                    }
                }
                if (parts[1] && parts[1].length > 3) {
                    parts[1] = parts[1].substring(0, 3);
                }
                return parts.join(",");
            };
            var applyFormatting = function () {
                var value = element.val();
                var original = value;
                if (!value || value.length == 0) { return }
                value = numberWithCommas(value);
                if (value != original) {
                    element.val(value);
                    element.triggerHandler('input')
                }
            };
            element.bind('keyup', function (e) {
                var keycode = e.keyCode;
                var isTextInputKey =
                  (keycode > 47 && keycode < 58) || // number keys
                  keycode == 32 || keycode == 8 || // spacebar or backspace
                  (keycode > 64 && keycode < 91) || // letter keys
                  (keycode > 95 && keycode < 112) || // numpad keys
                  (keycode > 185 && keycode < 193) || // ;=,-./` (in order)
                  (keycode > 218 && keycode < 223);   // [\]' (in order)
                if (isTextInputKey) {
                    applyFormatting();
                }
            });
            ngModelController.$parsers.push(function (value) {
                if (!value || value.length == 0) {
                    return value;
                }
                value = value.toString();
                value = value.replace(/[^0-9\,]/g, "");
                return value;
            });
            ngModelController.$formatters.push(function (value) {
                if (!value || value.length == 0) {
                    return value;
                }
                value = numberWithCommas(value, true);
                return value;
            });
        }
    };
});

// Directiva de autocompletado
app.directive('autocomplete', function () {
    var index = -1;
    return {
        restrict: 'E',
        scope: {
            data: '=data',
            onType: '=onType',
            confirmAction: '&',
            ngModel: '='
        },
        controller: function ($scope, $element, $attrs) {

            $scope.searchParam;
            $scope.searchParamOrig = angular.copy($scope.searchParam);

            // with the searchFilter the data get filtered
            $scope.searchFilter;

            // the index of the data that's currently selected
            $scope.selectedIndex = -1;

            // set new index
            $scope.setIndex = function (i) {
                $scope.selectedIndex = parseInt(i);
            }

            this.setIndex = function (i) {
                $scope.setIndex(i);
                $scope.$apply();
            }

            $scope.getIndex = function (i) {
                return $scope.selectedIndex;
            }

            // watches if the parameter filter should be changed
            var watching = true;

            // autocompleting drop down on/off
            $scope.completing = false;

            // starts autocompleting on typing in something
            $scope.$watch('searchParam', function () {
                if (watching && $scope.searchParam) {
                    $scope.completing = true;
                    $scope.searchFilter = $scope.searchParam;
                    $scope.selectedIndex = -1;
                }

                // function thats passed to on-type attribute gets executed
                if ($scope.onType)
                    $scope.onType($scope.searchParam);
            });

            // for hovering over data
            this.preselect = function (dato) {
                watching = false;
                $scope.$apply();
                watching = true;
            }

            $scope.preSelect = this.preSelect;

            this.preSelectOff = function () {
                watching = true;
            }

            $scope.preSelectOff = this.preSelectOff;

            // selecting a suggestion with RIGHT ARROW or ENTER
            $scope.select = function (dato) {
                if (dato) {
                    $scope.searchFilter = dato;
                    $scope.searchParam = null;
                    $scope.ngModel.codigo = dato.codigo;
                    if (dato.codigoPadre) {
                        $scope.ngModel.codigoPadre = dato.codigoPadre;
                    }
                    $scope.ngModel.nombre = dato.nombre;
                    $scope.confirmAction();
                }
                watching = false;
                $scope.completing = false;
                setTimeout(function () { watching = true; }, 1000);
                $scope.setIndex(-1);
            }
        },
        link: function ($scope, element, attrs) {

            $scope.placeholder = attrs["placeholder"];
            if ($scope.placeholder === null || $scope.placeholder === undefined)
                $scope.placeholder = "Comience a escribir..."
            if (attrs["clickActivation"] == "true") {
                element[0].onclick = function (e) {
                };
            }

            var key = { left: 37, up: 38, right: 39, down: 40, enter: 13, esc: 27 };

            document.addEventListener("keydown", function (e) {
                var keycode = e.keyCode || e.which;

                switch (keycode) {
                    case key.esc:
                        // disable data on escape
                        $scope.select();
                        $scope.setIndex(-1);
                        $scope.$apply();
                        e.preventDefault();
                }
            }, true);

            element[0].addEventListener("keydown", function (e) {
                var keycode = e.keyCode || e.which;

                var l = angular.element(this).find('li').length;

                // implementation of the up and down movement in the list of data
                switch (keycode) {
                    case key.up:

                        index = $scope.getIndex() - 1;
                        if (index < -1) {
                            index = l - 1;
                        } else if (index >= l) {
                            index = -1;
                            $scope.setIndex(index);
                            $scope.preSelectOff();
                            break;
                        }
                        $scope.setIndex(index);

                        if (index !== -1)
                            $scope.preSelect(angular.element(this).find('li')[index].innerText);

                        $scope.$apply();

                        break;
                    case key.down:
                        index = $scope.getIndex() + 1;
                        if (index < -1) {
                            index = l - 1;
                        } else if (index >= l) {
                            index = -1;
                            $scope.setIndex(index);
                            $scope.preSelectOff();
                            $scope.$apply();
                            break;
                        }
                        $scope.setIndex(index);

                        if (index !== -1)
                            $scope.preSelect(angular.element(this).find('li')[index].innerText);

                        break;
                    case key.left:
                        break;
                    case key.right:
                    case key.enter:

                        index = $scope.getIndex();
                        // scope.preSelectOff();
                        if (index !== -1)
                            $scope.select(angular.element(this).find('li')[index].innerText);
                        $scope.setIndex(-1);
                        $scope.$apply();

                        break;
                    case key.esc:
                        // disable data on escape
                        $scope.select();
                        $scope.setIndex(-1);
                        $scope.$apply();
                        e.preventDefault();
                        break;
                    default:
                        return;
                }

                if ($scope.getIndex() !== -1 || keycode == key.enter)
                    e.preventDefault();
            });

        },
        template: '<div class="autocomplete required">' +
                  '<input type="text" class="form-control" ng-model="searchParam" placeholder="{{placeholder}}" />' +
                  '<ul ng-show="completing" class="scrollable-ul-li" style="z-index: 4;">' +
                  '<li dato ng-repeat="dato in data | filter:searchFilter | orderBy:\'toString()\'" ' +
                  'index="{{$index}}" val="{{dato.codigo}}" ng-class="{active: ' +
                  '($index == selectedIndex)}" ng-click="select(dato)">' +
                  '{{dato.nombre}}' +
                  '</li>' +
                  '</ul>' +
                  '</div>'
    };
});

// Directiva para ayudar el autocompletar
app.directive('dato', function () {
    return {
        restrict: 'A',
        require: '^autocomplete', // ^look for controller on parents element
        link: function ($scope, element, attrs, autoCtrl) {
            element.bind('mouseenter', function () {
                autoCtrl.setIndex(attrs['index']);
            });

            element.bind('mouseleave', function () {
                autoCtrl.preSelectOff();
            });
        }
    }
});

app.directive('validDate', function ($timeout) {
  return {
    scope: {
      ngModel: '='
    },
    bindToController: true,
    controllerAs: 'vm',
    link: function (scope, element, attrs, ctrl) {

      element.on('blur', function () {
        // using timeout instead of scope.$apply, notify angular of changes
        $timeout(function () {
          ctrl.ngModel = ctrl.ngModel || new Date();
        });
      });
    }, 
    controller: function () {}
  }
});

app.directive(
    'dateInput',
    function(dateFilter) {
        return {
            require: 'ngModel',
            template: '<input type="date"></input>',
            replace: true,
            link: function(scope, elm, attrs, ngModelCtrl) {
                ngModelCtrl.$formatters.unshift(function (modelValue) {
                    return dateFilter(modelValue, 'yyyy-MM-dd');
                });

                ngModelCtrl.$parsers.unshift(function(viewValue) {
                    return new Date(viewValue);
                });
            },
        };
});