'use strict';

angular
  .module('datetimepicker', [])

  .provider('datetimepicker', function () {
    var default_options = {};

    this.setOptions = function (options) {
      default_options = options;
    };

    this.$get = function () {
      return {
        getOptions: function () {
          return default_options;
        }
      };
    };
  })

  .directive('datetimepicker', [
    '$timeout',
    'datetimepicker',
    function ($timeout,
              datetimepicker) {

      var default_options = datetimepicker.getOptions();

      return {
        require : '?ngModel',
        restrict: 'A',
        scope   : {
            datetimepickerOptions: '@',
            relatedAbajo: '@',
            relatedArriba: '@',
            minDateId: "@",
            actualizarLimites: "=",
            min:"="
        },
        link    : function ($scope, $element, $attrs, ngModelCtrl) {
          var passed_in_options = $scope.$eval($attrs.datetimepickerOptions);
          var options = jQuery.extend({}, default_options, passed_in_options);

          
          $scope.actualizarLimites = function () {

              if ($('#' + $attrs.minDateId).text() != undefined) {
                  setTimeout(function () {
                      $element.parent().data("DateTimePicker").minDate($('#' + $attrs.minDateId).text());
                  }, 100);
              } else {
                  setTimeout(function () {
                      $element.parent().data("DateTimePicker").minDate($('#' + $attrs.minDateId).value);
                  }, 100);
              }
              
              
          }

           $element.parent()
            .on('dp.change', function (e) {
              if (ngModelCtrl) {
                $timeout(function () {
                    ngModelCtrl.$setViewValue($element[0].value);
                });
                if ($element[0].value) {
                    if ($attrs.relatedAbajo) {
                        $('#' + $attrs.relatedAbajo).parent().data("DateTimePicker").minDate($element[0].value);
                    }
                    if ($attrs.relatedArriba) {
                        $('#' + $attrs.relatedArriba).parent().data("DateTimePicker").maxDate($element[0].value);
                    }
                } else {
                    if ($attrs.relatedAbajo) {
                        $('#' + $attrs.relatedAbajo).parent().data("DateTimePicker").minDate(false);
                    }
                    if ($attrs.relatedArriba) {
                        $('#' + $attrs.relatedArriba).parent().data("DateTimePicker").maxDate(false);
                    }
                }
              }
            })
            .datetimepicker(options);

           if ($scope.min != undefined) {

               $element.parent().data("DateTimePicker").minDate(new Date($scope.min));

           }


            $element.parent()
              .on('dp.hide', function (e) {
                  var self = $(this.children[0]);
                  setTimeout(function () {
                      self.focus();
                  }, 1);
              });

            $element.parent()
             .on('dp.error', function (e) {
                 ngModelCtrl.$setViewValue('');
             });

          function setPickerValue() {
            var date = null;

            if (ngModelCtrl && ngModelCtrl.$viewValue) {
              date = ngModelCtrl.$viewValue;
            }

              $element.parent()
              .data('DateTimePicker')
              .date(date);
          }

          if (ngModelCtrl) {
            ngModelCtrl.$render = function () {
              setPickerValue();
            };
          }

          setPickerValue();
        }
      };
    }
  ]);