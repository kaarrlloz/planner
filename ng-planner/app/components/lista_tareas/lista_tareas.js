var app = angular.module('app.tareas', ['restangular']);

app.controller('ListaTareasController', ['$http', '$rootScope', '$scope', 'ListaTareas', 'Restangular', 'Mensajes',
    function ($http, $rootScope, $scope, ListaTareas, Restangular, Mensajes) {

        function init() {
            var url = window.location;
            var urlseparada = url.href.split('/');
            var element = $('ul.nav a').filter(function () {
                if (this.href != "") {
                    var urlseparadaelemento = this.href.split('/');
                    return "verTareas".indexOf(urlseparadaelemento[urlseparadaelemento.length - 1]) == 0;
                }

            }).addClass('active').addClass('in').parent();
            if (element.is('li')) {
                element.addClass('active');
            }
        }

        init();


        $scope.nuevaTarea = {
            nombre: "",
            tipo: null,
            descripcion: ""
        };

        $scope.tareas = [];
        $scope.filtroTareas = {filtro:{ estado: "ACTIVA", tipo: {$eq:null} }}

         Restangular.all('tarea_filtro').post($scope.filtroTareas).then(
            function (result) {
                $scope.tareas = result;
                
            }
        );


        $scope.crearTarea = function () {
            Restangular.all('tarea').post($scope.nuevaTarea).then(
                function (result) {
                    $scope.tareas.push(result)
                    $scope.cancelar();

                    $('#nombreTarea').focus();
                    Mensajes.showMessage({ type: 0, msg: "<b>Sis-Planner</b> -- tu tarea fue guardada satisfactoriamente" });

                }
            ).catch(result => {
                Mensajes.showMessage({ type: 4, msg: "<b>Sis-Planner</b> -- Error al crear la tarea" });

            });
        }


        $scope.cancelar = function () {
            $scope.nuevaTarea = {
                nombre: "",
                tipo: null,
                descripcion: ""
            };
        }

    }]);