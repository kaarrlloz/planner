var app = angular.module('app.board', ['restangular']);

app.controller('BoardController', ['$http', '$rootScope', '$scope', 'ListaTareas', 'Restangular', 'Mensajes',
    function ($http, $rootScope, $scope, ListaTareas, Restangular, Mensajes) {

        $scope.filtroTareas = { filtro: { estado: "ACTIVA", tipo: "IU" } }

        $scope.chekbox = true;

        $scope.tareasUi = [];
        $scope.tareasINU = [];
        $scope.tareasNOU = [];
        $scope.tareasAsinadas = [];

        $scope.cargarTareas = function () {

            Restangular.all('tarea_filtro').post($scope.filtroTareas).then(
                function (result) {
                    $scope.tareasUi = result;
                }
            );

            $scope.filtroTareasINU = { filtro: { estado: "ACTIVA", tipo: "INU" } }
            Restangular.all('tarea_filtro').post($scope.filtroTareasINU).then(
                function (result) {
                    $scope.tareasINU = result;
                }
            );

            $scope.filtroTareasNOU = { filtro: { estado: "ACTIVA", tipo: "NOU" } }
            Restangular.all('tarea_filtro').post($scope.filtroTareasNOU).then(
                function (result) {
                    $scope.tareasNOU = result;
                }
            );

            $scope.filtroTareasAsinadas = { filtro: { estado: "ASIGNADA" } }
            Restangular.all('tarea_filtro').post($scope.filtroTareasAsinadas).then(
                function (result) {
                    $scope.tareasAsinadas = result;
                }
            );

            $scope.filtroTareasALD = { filtro: { estado: "ACTIVA", tipo: "ALD" } }
            Restangular.all('tarea_filtro').post($scope.filtroTareasALD).then(
                function (result) {
                    $scope.tareasALD = result;
                }
            );

        }

        $scope.cargarTareas();

        $scope.cerrarTareas = function (listaTareas) {
            var tareasSeleccionadas = [];
            for (i = 0; i < listaTareas.length; i++) {
                if (listaTareas[i].seleccionada == true) {
                    listaTareas[i].estado = "CERRADA"
                    tareasSeleccionadas.push(listaTareas[i]);
                }
            }
            if (tareasSeleccionadas.length != 0) {
                Restangular.all('tarealista').customPUT(tareasSeleccionadas).then(
                    function (result) {
                        Mensajes.showMessage({ type: 0, msg: result.message });
                        $scope.cargarTareas();
                    }
                ).catch(result => {
                    Mensajes.showMessage({ type: 4, msg: "<b>Sis-Planner</b> -- Error al actualizar la tarea" });
                });
            } else {
                Mensajes.showMessage({ type: 3, msg: "<b>Sis-Planner</b> -- No seleccionaste ninguna tarea para cerrar" });
            }


        }


        $scope.cancelar = function (listaTareas) {
            $scope.cargarTareas();
        }



    }]);