var app = angular.module('app.priorizacion', ['restangular']);

app.controller('PriorizacionController', ['$http', '$rootScope', '$scope', 'Restangular', 'Mensajes',
    function ($http, $rootScope, $scope, Restangular, Mensajes) {

        $scope.filtroTareas = {filtro:{ estado: "ACTIVA" }}

        $scope.tareas = [];

        Restangular.all('tarea_filtro').post($scope.filtroTareas).then(
            function (result) {
                $scope.tareas = result;
                $scope.tareas.forEach(item => {
                    if(item.fechaPlaneada != undefined)
                    item.fechaPlaneada = new Date(moment(item.fechaPlaneada).format('YYYY-MM-DD'));
                })
            }
        );

        $scope.editarTarea = function (tarea) {
            tarea.edit = true;

        }


        $scope.cancelTarea = function (tarea) {
            tarea.edit = false;

        }

        $scope.actualizarTarea = function (tarea) {
            tarea.edit = false;
            Restangular.all('tarea/' + tarea._id).customPUT(tarea).then(
                function (result) {
                    Mensajes.showMessage({ type: 0, msg: "<b>Sis-Planner</b> -- tu tarea fue actualizada satisfactoriamente" });
                }
            ).catch(result => {
                Mensajes.showMessage({ type: 4, msg: "<b>Sis-Planner</b> -- Error al actualizar la tarea" });
            });
        }

        $scope.eliminarTarea = function (tarea) {
            tarea.edit = false;
            Restangular.all('tarea/' + tarea._id).customDELETE().then(
                function (result) {
                    Mensajes.showMessage({ type: 0, msg: "<b>Sis-Planner</b> -- tu tarea fue eliminada satisfactoriamente" });

                    $scope.position = null;
                    for (i = 0; i < $scope.tareas.length; i++) {
                        if ($scope.tareas[i]._id == tarea._id) {
                            $scope.position = i;
                            break;
                        }
                    }
                    $scope.tareas.splice($scope.position, 1);
                }
            ).catch(result => {
                Mensajes.showMessage({ type: 4, msg: "<b>Sis-Planner</b> -- Error al eliminar la tarea" });
            });

        }


        $scope.asignarTarea = function (tarea) {
            tarea.edit = false;
            var tareaAsignada = {estado: "ASIGNADA"}
            Restangular.all('tarea/' + tarea._id).customPUT(tareaAsignada).then(
                function (result) {
                    Mensajes.showMessage({ type: 0, msg: "<b>Sis-Planner</b> -- tu tarea fue asignada" });

                    $scope.position = null;
                    for (i = 0; i < $scope.tareas.length; i++) {
                        if ($scope.tareas[i]._id == tarea._id) {
                            $scope.position = i;
                            break;
                        }
                    }
                    $scope.tareas.splice($scope.position, 1);

                }
            ).catch(result => {
                Mensajes.showMessage({ type: 4, msg: "<b>Sis-Planner</b> -- Error al asignar la tarea" });
            });
        }

    }]);