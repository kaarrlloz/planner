var app = angular.module('app.seguimiento', ['restangular']);

app.controller('SeguimientoController', ['$http', '$rootScope', '$scope', 'ListaTareas', 'Restangular', 'Mensajes',
    function ($http, $rootScope, $scope, ListaTareas, Restangular, Mensajes) {

        $scope.nuevoCoentario = {
            descripcion: "",
            id_tarea: null
        }

        $scope.seguimiento = angular.copy($scope.nuevoCoentario);

        $scope.tareaSeleccionada = {};

        $scope.filtroTareas = { filtro: { estado: "ASIGNADA" } }

        $scope.tareas = [];

        Restangular.all('tarea_filtro').post($scope.filtroTareas).then(
            function (result) {
                $scope.tareas = result;
                $scope.tareas.forEach(item => {
                    if (item.fechaPlaneada != undefined)
                        item.fechaPlaneada = new Date(moment(item.fechaPlaneada).format('YYYY-MM-DD'));
                })
            }
        );

        $scope.seleccionarTarea = function (tarea) {
            $scope.tareaSeleccionada = angular.copy(tarea);

            Restangular.all('seguimiento/' + $scope.tareaSeleccionada._id).customGET("", "").then(
                function (result) {
                    $scope.tareaSeleccionada.comentarios = result;
                }
            );

            $scope.seguimiento = angular.copy($scope.nuevoCoentario);
            $scope.seguimiento.id_tarea = $scope.tareaSeleccionada._id

        }

        $scope.agregarSeguimiento = function () {
            Restangular.all('seguimiento').post($scope.seguimiento).then(
                function (result) {
                    $scope.tareaSeleccionada.comentarios.push(result)
                    $('#descrip').focus();
                    Mensajes.showMessage({ type: 0, msg: "<b>Sis-Planner</b> -- Comentario agregado" });
                    $scope.seguimiento.descripcion = null;

                }
            ).catch(result => {
                Mensajes.showMessage({ type: 4, msg: "<b>Sis-Planner</b> -- Error al crear el comentario" });

            });
        }

         $scope.actualizarTarea = function () {
            var nuevoEstado = { estado: "CERRADA" };

            Restangular.all('tarea/' + $scope.seguimiento.id_tarea).customPUT(nuevoEstado).then(
                function (result) {
                    Mensajes.showMessage({ type: 0, msg: "<b>Sis-Planner</b> -- tu tarea fue actualizada satisfactoriamente" });
                    $('#detalleModal').modal('toggle');
                }
            ).catch(result => {
                Mensajes.showMessage({ type: 4, msg: "<b>Sis-Planner</b> -- Error al actualizar la tarea" });
            });
        }

    }]);