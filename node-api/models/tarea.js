'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const TareaSchema = Schema({
    nombre: {type: String, required: true},
    descripcion: {type: String},
    tipo: {type: String, enum: ['IU','INU','NOU','NONU','ALD',null], default: null},
    fechaRegistro: Date,
    estado: {type: String, enum: ['ACTIVA','CERRADA', 'ASIGNADA'], required: true, default: 'ACTIVA'},
    fechaPlaneada: Date,
    duracionEstimada: Number,
    id_usuario:{type: String, required: true}
}) 

module.exports = mongoose.model('Tarea', TareaSchema)