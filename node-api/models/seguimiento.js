'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const SeguimientoSchema = Schema({
    descripcion: {type: String},
    fechaRegistro: Date,
    id_tarea:{type: String, required: true},
    id_usuario:{type: String, required: true},
}) 

module.exports = mongoose.model('Seguimiento', SeguimientoSchema)