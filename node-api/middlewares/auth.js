'use strict'

const service = require('../services')

function isAuth(req, res, next) {
    console.log("verificacion de token --")
    if(!req.headers.authorization){
        return res.status(403).send({message:`Usuario no autenticado`})
    }

    const token = req.headers.authorization.split(" ")[1]

    service.decodeToken(token).then(response => {
        req.user = response
        next()
    }).catch( response => {

        console.log("token - Mal-" + response.status)
        res.status(response.status).send({message:"Token no valido"})
    })
}

module.exports = isAuth