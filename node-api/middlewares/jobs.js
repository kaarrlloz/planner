
'use strict'

var CronJob = require('cron').CronJob;
const userCtrl = require('../controllers/user')
const tareaCtrl = require('../controllers/tarea')
const moment = require('moment')
const service = require('../services')

/*
   * Corre todas las semanas (Lunes, miercoles y viernes)
   * a las 11:30:00 AM.
  */

new CronJob('00 05 10 * * 1,3,5', function () {
    var usuariosConsultados = userCtrl.getUsuarios()
    usuariosConsultados.on('data', function (usuario) {

        var hoy = moment();
        var ultimaConeccion = moment(usuario.signupDate)
        var diferencia = hoy.diff(ultimaConeccion, "days");

        if (diferencia > 2) {
            var mailOptions = {
                template: "recordatorio",
                to: usuario.email,
                subject: '¿Cual es tu progreso esta semana?',
                user: {
                    name: usuario.nombre
                }
            }
            service.enviarNotificacion(mailOptions);
        }
    })


}, null, true, 'America/Bogota');


/*
   * Corre todos los dias
   * a las 08:30:00 AM.
  */

new CronJob('00 05 08 * * *', function () {
    var tareas = tareaCtrl.getTareasAbiertas()
    var mapaTareas = {};
    var keys = [];
    tareas.on('data', function (tarea) {
        var hoy = moment();
        var fechaplaneada = moment(tarea.fechaPlaneada)
        var diferencia = hoy.diff(fechaplaneada, "days");

        if (diferencia <= 2) {
            if (mapaTareas[tarea.id_usuario] == undefined) {
                keys.push(tarea.id_usuario);
                mapaTareas[tarea.id_usuario] = []
                mapaTareas[tarea.id_usuario].push(tarea);
            } else {
                mapaTareas[tarea.id_usuario].push(tarea);
            }
        }
    })

    tareas.on('close', function () {
        keys.forEach(function (key) {
            var usuario = userCtrl.getUsuarioByid(key)
            usuario.on('data', function (usuario) {
                var mailOptions = {
                    template: "recordatorio",
                    to:usuario.email,
                    subject: 'Tienes tareas proximas a vencerse',
                    tareas: mapaTareas[key],
                    moment: require( 'moment' ),
                    user: {
                        name: usuario.nombre,                      
                    }
                }
                service.enviarNotificacion(mailOptions);
            })
        })

    });
}, null, true, 'America/Bogota');
