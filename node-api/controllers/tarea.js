'use strict'

const Tarea = require('../models/tarea')
const service = require('../services')
const User = require('../models/user')

function getTarea(req, res) {

    let idTare = req.params.tareaId
    Tarea.findById(idTare, (err, tarea) => {
        if (err) return res.status(500).send({ message: `Error consultando la BD` })
        if (!tarea) return res.status(400).send({ message: `El producto no existe` })

        res.status(200).send(tarea)
    })

}

function getTareas(req, res) {
    Tarea.find({ id_usuario: req.user }, (err, tareas) => {
        if (err) return res.status(500).send({ message: `Error consultando las tareas` })
        if (!tareas) return res.status(400).send({ message: `No hay tareas almacenadas` })

        res.status(200).send(tareas)
    })
}

function getTareasFiltro(req, res) {
    let filtro = req.body.filtro
    console.log(filtro)
    if (filtro == undefined) filtro = {}
    filtro.id_usuario = req.user
    Tarea.find(filtro, (err, tareas) => {
        if (err) return res.status(500).send({ message: `Error consultando las tareas` })
        if (!tareas) return res.status(400).send({ message: `No hay tareas almacenadas` })

        res.status(200).send(tareas)
    })
}

function setTarea(req, res) {

    let tarea = new Tarea()
    tarea.nombre = req.body.nombre
    tarea.descripcion = req.body.descripcion
    tarea.tipo = req.body.tipo
    tarea.fechaRegistro = new Date()
    tarea.id_usuario = req.user

    tarea.save((err, tareaStored) => {
        if (err) {
            res.status(500).send({ message: `Error al almacenar la tarea ${err}!` })
        } else {
            res.status(200).send(tareaStored)

            User.findById(req.user, (err, respuesta) => {

                if (err) return;
                var mailOptions = {
                    template: "tarea",
                    to: respuesta.email,
                    subject: 'Sis - Planner nueva tarea!',
                    user: {  // data to view template, you can access as - user.name
                        name: respuesta.nombre,
                        tareaname: tarea.nombre
                    }
                }

                service.enviarNotificacion(mailOptions);
            })


        }
    })
}

function updateTarea(req, res) {
    let idTare = req.params.tareaId
    let update = req.body

    Tarea.findByIdAndUpdate(idTare, update, (err, tareaUpdate) => {
        if (err) return res.status(500).send({ message: `Error consultando la BD` })
        if (!tareaUpdate) return res.status(400).send({ message: `El producto no fue actualizado` })
        res.status(200).send(tareaUpdate)

    })

}

function updateTareas(req, res) {
    var listaTareas = req.body;
    var i = 0;
    for (i = 0; i < listaTareas.length; i++) {
        let idTare = listaTareas[i]._id
        let update = {}
        update.estado = listaTareas[i].estado
        Tarea.findByIdAndUpdate(idTare, update, (err, tareaUpdate) => {
            if (err)   console.log("ocurrio un error--" + err)
            if (!tareaUpdate)  console.log("Error actualizando las tareas")
            
        })
    }

    res.status(200).send({ message: "Tareas actualizadas con exito" })
}

function deleteTarea(req, res) {
    let idTare = req.params.tareaId
    Tarea.findById(idTare, (err, tarea) => {
        if (err) return res.status(500).send({ message: `Error consultando la BD` })
        if (!tarea) return res.status(400).send({ message: `El producto no existe` })

        tarea.remove(err => {
            if (err) return res.status(500).send({ message: `Error consultando la BD` })
            res.status(200).send({ message: "Tarea eliminada con exito" })
        })

    })

}


function getTareasAbiertas() {
    let filtro = {estado:"ACTIVA", fechaPlaneada: {$ne:null}}

    var tarea = Tarea.find(filtro, (err, tareas) => {
        if (err) console.log("Error al consultar los usuarios")
        if (!tareas) console.log("Error al consultar los usuarios")
    }).cursor();

    return tarea
}


module.exports = {
    getTarea,
    getTareas,
    setTarea,
    updateTarea,
    updateTareas,
    deleteTarea,
    getTareasFiltro,
    getTareasAbiertas
}