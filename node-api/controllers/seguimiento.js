'use strict'

const Seguimiento = require('../models/seguimiento')



function getSeguimientos( req, res){
    Seguimiento.find({id_tarea:req.params.tareaId,id_usuario:req.user}, (err, seguimientos) => {
            if(err) return res.status(500).send({message:`Error consultando Los Seguimientos`})
            if(!seguimientos) return res.status(400).send({message:`No hay Seguimientos`})

            res.status(200).send(seguimientos)
        })
}



function setSeguimiento( req, res){
    
    let seguimiento = new Seguimiento()
    seguimiento.fechaRegistro = new Date()
    seguimiento.id_usuario = req.user
    seguimiento.id_tarea = req.body.id_tarea
    seguimiento.descripcion = req.body.descripcion


    seguimiento.save((err, seguimientoStored) => {
        if(err){
            res.status(500).send({message:`Error al almacenar seguimiento ${err}!`})
        } else {
            res.status(200).send(seguimientoStored)
            
        }
    })
}



module.exports = {
    getSeguimientos,
    setSeguimiento
}