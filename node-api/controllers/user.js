'use strict'

const mongoose = require('mongoose')
const User = require('../models/user')
const service = require('../services')

function signUp(req, res) {

    const user = new User({
        email: req.body.email,
        nombre: req.body.nombre,
        password: req.body.password
    })

    user.save((err) => {
        if (err) res.status(500).send({ message: `Error al crear el usuario` })
        res.status(201).send({ token: service.createToken(user),nombre: user.nombre, })
    })


}

function signIn(req, res) {

    if (!req.body.email) return res.status(404).send({ message: "Usuario no encontrado" })
    User.find({ email: req.body.email }, (err, userFind) => {
        if (err) return res.status(500).send({ message: err })
        if (userFind.length == 0) return res.status(404).send({ message: "Usuario no encontrado" })

        if (req.body.password != undefined && req.body.password == userFind[0].password) {
            req.user = userFind
            res.status(200).send({
                message: "Login exitoso",
                nombre: userFind[0].nombre,
                token: service.createToken(userFind[0])
            })
        } else {
            res.status(401).send({
                message: "Usuario o contraseña incorrectos"
            })
        }

    })
}

function getUsuarios() {
    var usuarios = User.find((err, users) => {
        if (err) console.log("Error al consultar los usuarios")
        if (!users) console.log("no hay usuarios");
    }).cursor();

    return usuarios;
}

function getUsuarioByid(idUser) {
    var usuarios = User.findById(idUser, (err, respuesta) => {
        if (err) console.log("Error al consultar los usuarios")
        if (!respuesta) console.log("no hay usuarios");
    }).cursor();

    return usuarios;
}


module.exports = {
    signUp,
    signIn,
    getUsuarios,
    getUsuarioByid
}