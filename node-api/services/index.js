'use strict'

const jwt = require('jwt-simple')
const moment = require('moment')
const config = require('../config')
const express = require('express')
const mailer = require('express-mailer');

const mailapp = express()
mailapp.set('views', 'services');
mailapp.set('view engine', 'jade');

mailer.extend(mailapp, {
  from: 'sisplanner.notify@gmail.com',
  host: 'smtp.gmail.com', // hostname 
  secureConnection: true, // use SSL 
  port: 465, // port for secure SMTP 
  transportMethod: 'SMTP', // default is SMTP. Accepts anything that nodemailer accepts 
  auth: {
    user: 'sisplanner.notify@gmail.com',
    pass: '7l8o45gv'
  }
});


function createToken(user) {
    const payLoad = {
        sub: user._id,
        iat: moment.unix(),
        exp: moment().add(14,'days').unix()
    }

   return jwt.encode(payLoad, config.secret)
}

function decodeToken (token){
    const decode = new Promise((resolve, reject) =>{
        try {
            const payload = jwt.decode(token,config.secret)
            //console.log("este es el PAYLOAD: "+ payload)
            if(payload.exp <= moment.unix()){
                reject({
                   status:401,
                   message: "El token a expirado" 
                })    
            }

            resolve(payload.sub)

        } catch (err){
            reject({
                status: 401,
                message: 'token invalido'
            })
        }
    })

    return decode
}


function enviarNotificacion(mailOptions){

    mailapp.mailer.send(mailOptions.template, mailOptions, function (err, message) {
    if (err) {
      console.log(err);
      return;
    }
     console.log('Email has been sent!');
  });

}
module.exports = {createToken,decodeToken,enviarNotificacion }
