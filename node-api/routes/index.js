'use strict'

const express = require('express')
const api = express.Router()

const tareaCtrl = require('../controllers/tarea')
const userCtrl = require('../controllers/user')
const segCtrl = require('../controllers/seguimiento')


const auth = require('../middlewares/auth')


api.get('/tarea', auth, tareaCtrl.getTareas)
api.get('/tarea/:tareaId',auth, tareaCtrl.getTarea)

api.post('/tarea_filtro', auth, tareaCtrl.getTareasFiltro)

api.post('/tarea',auth, tareaCtrl.setTarea)
api.put('/tarea/:tareaId',auth, tareaCtrl.updateTarea)
api.put('/tarealista',auth, tareaCtrl.updateTareas)
api.delete('/tarea/:tareaId', auth, tareaCtrl.deleteTarea)


api.get('/seguimiento/:tareaId',auth, segCtrl.getSeguimientos)
api.post('/seguimiento', auth, segCtrl.setSeguimiento)


api.post('/signUp', userCtrl.signUp)
api.post('/signIn', userCtrl.signIn)





module.exports = api