'use strict'

const express = require('express')
const bodyParser = require('body-parser')


const app = express()
const api = require('./routes')
const jobs = require('./middlewares/jobs')

const tareaCtrl = require('./controllers/tarea')

app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, authorization");
  res.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
  next();
});

app.use('/api', api)

module.exports = app